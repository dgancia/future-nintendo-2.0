import os
from auth.register import register
from auth.reset_password import reset_password


def clear():
    os.system('clear||cls')

def logout():
    return True

def menu():

    print(">","="*102,"<")
    print("|      __   __   __    ________    __          ________    _______    ____________    ________    __     |")
    print("|     |  | |  | |  |  /   _____|  |  |        /   _____|  /   _   \  /   _    _   \  /   _____|  |  |    |")
    print("|     |  | |  | |  |  |  |____    |  |        |  |        |  | |  |  |  | |  | |  |  |  |____    |  |    |")
    print("|     |  | |  | |  |  |   ____|   |  |        |  |        |  | |  |  |  | |  | |  |  |   ____|   |__|    |")
    print("|     |  |_|  |_|  |  |  |_____   |  |_____   |  |_____   |  |_|  |  |  | |  | |  |  |  |_____    __     |")
    print("|     \____________/  \________|  \________|  \________|  \_______/  |__| |__| |__|  \________|  |__|    |")
    print("|","                                                                                                       |")

    print(">","="*102,"<")
    print("|",'                                          [1] Register' ,"                                                |")
    print("|",'                                          [2] Login' ,"                                                   |")
    print("|",'                                          [3] Logout' ,"                                                  |")
    print("|",'                                          [4] Reset Password' ,"                                          |")
    print("|",'                                          [5] Exit' ,"                                                    |")
    print(">","="*102,"<")

    global main_menu_input
    main_menu_input = input('==> Choice: ')
    is_exit = False

    while is_exit == False:
        if main_menu_input == '1':
            clear()
            register()
            menu()
        if main_menu_input == '2':
            clear()
            from auth.login import login
            login()
        if main_menu_input == '3':
            clear()
            logout()
            menu()
        if main_menu_input == '4':
            clear()
            reset_password()
            menu()
        if main_menu_input == '5':
            clear()
            exit()
        
        return True

menu()
    

